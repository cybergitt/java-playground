public class PassArray {
    public static void main(String...args)
    {
        final int SIZE = 10; // Size of the array

        // Create an array.
        int[] list = new int[SIZE];

        fillArray(list);

        int max = 0; // hold index number of largest number
        // Finding largest value.
        max = getMax(list);

        // Show all elements of array
        System.out.print("Elements are : ");

        printArray(list);

        System.out.println();

        // Show largest value of array
        System.out.println("Largest value is " + list[max]);
    }

    private static void fillArray(int[] list)
    {
        // Fill array with random values
        for (int i = 0; i < list.length; i++) {
            list[i] = (int) (Math.random() * 100);
        }
    }

    private static int getMax(int[] list)
    {
        int max = 0;
        for (int i = 0; i < list.length; i++) {
            // if (list[i] > list[max]) {
            //     max = i;
            // }
            max = (list[i] > list[max]) ? i : max;
        }
        return max;
    }

    private static void printArray(int[] list)
    {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i] + " ");
        }
    }
}
