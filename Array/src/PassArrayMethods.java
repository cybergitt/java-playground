import java.util.Scanner;

public class PassArrayMethods {
    public static void main(String...args)
    {
        final int SIZE = 4; // Size of the array

        // create an array
        int[] arr = new int[SIZE];

        // Pass the array to the fillArray method
        fillArray(arr, SIZE);
        System.out.println("The numbers are : ");

        // Pass the array to the printArray method
        printArray(arr, SIZE);
    }

    /**
     * the fillArray method accepts an array as
     * an argument. Each of its element is filled by user
     */
    private static void fillArray(int[] list, int size) {
        // Create a scanner object for keyboard input
        Scanner console = new Scanner(System.in);

        System.out.println("Enter " + size + " integers");
        for (int i = 0; i < size; i++) {
            list[i] = console.nextInt();
        }
    }

    /**
     * The printArray method accepts an array 
     * as an argument displays its contents.
     * @param list
     * @param size
     */
    private static void printArray(int[] list, int size) {
        for (int i = 0; i < size; i++) {
            System.out.println(list[i] + " ");
        }
    }
}
