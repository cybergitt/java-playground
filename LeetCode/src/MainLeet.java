import java.util.*;

public class MainLeet {
    public static void main(String[] args) throws Exception {
        int[] nums = { 0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        // int[] nums = { 3, 3, 2 };
        // int[] nums = {3, 2, 2, 3};
        // int[] nums = {3, 3};
        // int[] nums = {3};

        // int len = removeDuplicatesFix2(nums);
        int len = removeElement(nums, 3);

        for (int i = 0; i < len; i++) {
            // print(nums[i]);
            System.out.print(nums[i] + " ");
        }
    }

    private static int removeDuplicates(int[] nums) {
        int len = nums.length;

        if ( len < 1) {
            return len;
        }
        for (int i = 0; i < len; i++) {
            // int shift = (i == len) ? len : i + 1;
            int shift = i + 1;
            if (nums[i] == nums[shift]) {
                shrinkArray(nums, shift, len);
                len--;
            } else {
                len++;
            }
        }
        return len;
    }

    private static int removeDuplicates2(int[] nums) {
        int len = nums.length, temp;

        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                if (nums[i] == nums[j]) {
                    shrinkArray(nums, i, len);
                    len--;
                } 
            }
        }
        
        return len;
    }

    private static int removeDuplicatesFix(int[] nums) {
        if (nums.length == 0)
            return 0;
        int i = 0;
        for (int j = 1; j < nums.length; j++) {
            if (nums[j] != nums[i]) {
                i++;
                nums[i] = nums[j];
            }
        }
        return i + 1;
    }

    private static int removeDuplicatesFix2(int[] spaces) {
        // set initiation length
        int len = spaces.length;

        if (len == 0)
            return 0; // return back the zero length

        int shifter = 0; // initiate the shifteror pointer 1
        for (int finder = 1; finder < spaces.length; finder++) { // set finder as pointer 2 act as duplicate finder
            if (spaces[finder] != spaces[shifter]) { // if the current space value is not same to the next space value
                shifter++; // shifter find the doppleganger to the next space
                spaces[shifter] = spaces[finder]; // current space is filled by the next space
            } else {
                len--; // decrease the space quota
            }
        }

        return len; // turn back the latest space quota
    }

    private static void printArrayInt(int[] array) {
        for (int i : array) {
            System.out.print(i + ", ");
        }
    }

    private static void shrinkArray(int[] arr, int pos, int size) {
        for (int i = pos; i < size - 1; i++) {
            arr[i] = arr[i + 1];
        }
    }

    public static int removeElement(int[] nums, int val) {
        int len = nums.length;
        if (len == 0)
            return 0;

        int i = 0;
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] != val) {
                nums[i] = nums[j];
                i++;
            } else {
                len--;
            }
        }

        return len;
    }
}
